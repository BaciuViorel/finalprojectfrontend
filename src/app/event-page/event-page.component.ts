import { Component } from '@angular/core';
import { Event } from "src/app/model/event";
import {HttpClient} from "@angular/common/http";
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.css']
})
export class EventPageComponent {
   event: Event = new Event( null, null, null, null, null, null, null);

  httpClient: HttpClient;
  route: ActivatedRoute

  constructor(httpClient: HttpClient , route : ActivatedRoute, private router : Router){
      this.httpClient = httpClient;
      this.route= route;
  }

  /* aici primi raspunsul de la server */
  ngOnInit(){
    const eventId =  this.route.snapshot.paramMap.get("id");  /*sintaxa pentru a accesa parametrul din url */
    this.httpClient.get("/api/events/" + eventId).subscribe((response)=>{
      console.log(response); 
      this.event = response as Event;
      if (this.event.startDate != null)
          this.event.startDate = new Date(this.event.startDate!);
        if (this.event.endDate != null)
          this.event.endDate = new Date(this.event.endDate!);
    },
    (error) => {
      console.log(error);
      if(error.error == "There is not event with ID "+ eventId){
        this.router.navigate(["not-found"])
        }
      });

    // .subscribe(succes, error)
    // .subscribe(() => {}, () => {})
    // .subscribe (succes, => (error) =>)
  }
}
